

class PrinterCounter:

    @staticmethod
    def execute(
            *,
            set_with_school_total: set,
            count_school_total: int,
            count_school_per_state: dict,
            count_school_fer_city: dict,
            city_with_more_school: list,
            city_with_unique_school: int
    ):
        print(f"Total school: {count_school_total}")
        print(f"Schools by State:")
        for state, value in count_school_per_state.items():
            print(f"{state}: {value}")
        print(f"School by Metro-centric locale:")
        for index, value in enumerate(count_school_fer_city.items()):
            print(f"{index + 1}: {value[1]}")
        print(f"City with most schools: {city_with_more_school[0]} ({city_with_more_school[1]} schools)")
        print(f"Unique cities with at least one school: {city_with_unique_school}")


class PrinterSearch:

    def __init__(self):
        self.city_column_name = "LCITY05"
        self.state_column_name = "LSTATE05"
        self.school_name_column_name = 'SCHNAM05'

    def execute(self, *, best_three_names_found: list, school_information: str):
        print(f"""Result for "{school_information}".""")
        for index, school_line in enumerate(best_three_names_found):
            print(f"{index + 1}. {school_line.get(self.school_name_column_name)}")
            print(f"{school_line.get(self.city_column_name)}, {school_line.get(self.state_column_name)}")
