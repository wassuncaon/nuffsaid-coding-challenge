from typing import Any

from views import PrinterCounter
from models import CSV


class CountSchool:

    def __init__(self):
        self.csv = CSV()
        self.city_column_name = "LCITY05"
        self.state_column_name = "LSTATE05"
        self.school_id_column_name = "LEAID"
        self.school_name_column_name = 'SCHNAM05'
        self.set_with_school_total, self.count_school_total = self.get_count_school_total()
        self.count_school_per_state = self.get_count_school_by_state()
        self.count_school_fer_city = self.get_count_school_by_city()
        self.city_with_more_school, self.city_with_unique_school = self.get_city_with_more_school_and_unique()
        self.printer = PrinterCounter()

    def get_count_school_total(self) -> tuple[set[Any], int]:
        csv_read = self.csv.read()
        counter_school_set = set()
        for line in csv_read:
            counter_school_set.add(line.get(self.school_name_column_name))
        return counter_school_set, len(counter_school_set)

    def get_count_school_by_state(self) -> dict:
        csv_read = self.csv.filter_by_column(self.state_column_name)
        return self._get_count_school_generic(csv_read)

    def get_count_school_by_city(self) -> dict:
        csv_read = self.csv.filter_by_column(self.city_column_name)
        return self._get_count_school_generic(csv_read)

    def _get_count_school_generic(self, csv_read) -> dict:
        school_count = {}
        for key, value in csv_read.items():
            school_set_list = set()
            for item in value:
                school_set_list.add(item.get(self.school_id_column_name))
            school_count.update({key: len(school_set_list)})
        return school_count

    def get_city_with_more_school_and_unique(self) -> tuple[list[Any], int]:
        dict_by_city = self.count_school_fer_city
        max_value_of_school_in_city = 0
        city_name_with_max_value = None
        unique_cities_with_at_least_one_school = 0
        for city, value in dict_by_city.items():
            if value > max_value_of_school_in_city:
                max_value_of_school_in_city = value
                city_name_with_max_value = city
            if value > 0:
                unique_cities_with_at_least_one_school += 1
        return [city_name_with_max_value, max_value_of_school_in_city], unique_cities_with_at_least_one_school

    def print_count(self):
        self.printer.execute(
            set_with_school_total=self.set_with_school_total,
            count_school_total=self.count_school_total,
            count_school_per_state=self.count_school_per_state,
            count_school_fer_city=self.count_school_fer_city,
            city_with_more_school=self.city_with_more_school,
            city_with_unique_school=self.city_with_unique_school
        )
