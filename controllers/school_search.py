import re

from views import PrinterSearch
from helper import timeit
from models import CSV


class SchoolSearch:

    def __init__(self):
        self.csv = CSV()
        self.printer = PrinterSearch()
        self.city_column_name = "LCITY05"
        self.state_column_name = "LSTATE05"
        self.school_name_column_name = 'SCHNAM05'

    @timeit
    def search_schools(self, school_information: str) -> list:
        city_school_state_names_list = self.csv.get_city_school_state_names_list()
        # school_information_list = self._split_compound_name([school_information])
        school_name = self._find_school_name(school_information, city_school_state_names_list)
        self.printer.execute(best_three_names_found=school_name, school_information=school_information)
        return school_name

    def _find_school_name(self, school_information: list, csv_information: list) -> list:
        best_three_names_found = []
        for line in csv_information:
            if any(
                    name.upper().strip() in line.get(self.school_name_column_name).upper().strip()
                    for name in school_information
            ):
                best_three_names_found.append(line)
                if len(best_three_names_found) == 3:
                    break
        return best_three_names_found

    def _split_compound_name(self, name_list: list) -> list:
        for name in name_list:
            if self._to_verify_the_name_is_compound(name):
                return name.split()
        return name_list

    @staticmethod
    def _to_verify_the_name_is_compound(name: str) -> bool:
        return bool(re.search(r"\s", name))
