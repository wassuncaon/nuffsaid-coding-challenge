import csv
import itertools
import operator


class CSV:

    def __init__(self):
        self.csv_read = self.read()
        self.city_column_name = "LCITY05"
        self.state_column_name = "LSTATE05"
        self.school_name_column_name = 'SCHNAM05'


    @staticmethod
    def read():

        with open('models/school_data.csv', mode='r') as file:
            opened_file = csv.DictReader(file, delimiter=',')
            try:
                return [*opened_file]
            except UnicodeError as e:
                return [*opened_file]

    def filter_by_column(self, column):
        csv_read = self.csv_read
        resultado = {}
        del csv_read[0]
        for k, g in itertools.groupby(csv_read, key=operator.itemgetter(column)):
            resultado.update({k: list(g)})
        return resultado

    def get_city_school_state_names_list(self):
        csv_read = self.csv_read
        resultado = []
        for item in csv_read:
            resultado.append({
                self.school_name_column_name: item.get(self.school_name_column_name),
                self.city_column_name: item.get(self.city_column_name),
                self.state_column_name: item.get(self.state_column_name),
            })
        return resultado
