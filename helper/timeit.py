from functools import wraps
import time


def timeit(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        srtat_time = time.perf_counter()
        try:
            return func(*args, **kwargs)
        finally:
            end_time = time.perf_counter()
            total_time_milliseconds = (end_time - srtat_time)
            message = f"(search took: {total_time_milliseconds:.5f}s)"
            print(message)

    return wrapper
